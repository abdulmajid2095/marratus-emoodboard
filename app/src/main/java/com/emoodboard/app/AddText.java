package com.emoodboard.app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import java.io.Serializable;

public class AddText extends AppCompatActivity {

    EditText editText;
    CardView selectFont, add;
    TextView text;
    LinearLayout selectColor;
    int selectedColor = -16777216;

    Typeface type1, type2, type3, type4, type5, type6, type7, type8, type9, type10;
    int selectedFont = 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_text);

        editText = findViewById(R.id.editText);
        selectFont = findViewById(R.id.selectFont);
        text = findViewById(R.id.text);
        selectColor = findViewById(R.id.selectColor);
        add = findViewById(R.id.add);

        type1 = Typeface.createFromAsset(getAssets(), "f1_barika.ttf");
        type2 = Typeface.createFromAsset(getAssets(), "f2_caviar_dreams.ttf");
        type3 = Typeface.createFromAsset(getAssets(), "f3_coolvetica.ttf");
        type4 = Typeface.createFromAsset(getAssets(), "f4_humble_boys.otf");
        type5 = Typeface.createFromAsset(getAssets(), "f5_keepon_truckin.ttf");
        type6 = Typeface.createFromAsset(getAssets(), "f6_milky_nice.ttf");
        type7 = Typeface.createFromAsset(getAssets(), "f7_roboto.ttf");
        type8 = Typeface.createFromAsset(getAssets(), "f8_secret_admire.ttf");
        type9 = Typeface.createFromAsset(getAssets(), "f9_sketch_3d.otf");
        type10 = Typeface.createFromAsset(getAssets(), "f10_vogue.ttf");

        text.setTypeface(type7);

        selectFont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFont();
            }
        });

        selectColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                colorDialog();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = ""+editText.getText().toString();
                if(text.equals("") || text.isEmpty())
                {
                    editText.setError("Text cannot be empty");
                }
                else
                {
                    Intent intentx = new Intent();
                    intentx.putExtra("text", ""+text);
                    intentx.putExtra("color", selectedColor);
                    intentx.putExtra("font", selectedFont);
                    setResult(Activity.RESULT_OK,intentx);
                    finish();
                }
            }
        });
    }

    public void colorDialog()
    {
        ColorPickerDialogBuilder
                .with(this)
                .setTitle("Pilih Warna")
                .initialColor(Color.WHITE)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setOnColorSelectedListener(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int selectedColor) {
                        //Toast.makeText(getApplicationContext(), "onColorSelected: 0x" + Integer.toHexString(selectedColor), Toast.LENGTH_SHORT).show();
                    }
                })
                .setPositiveButton("Pilih", new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int mySelectedColor, Integer[] allColors) {
                        selectedColor = mySelectedColor;
                        selectColor.setBackgroundColor(mySelectedColor);
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .build()
                .show();
    }

    public void dialogFont() {
        final DialogPlus dialogFont = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.model_dialog_font))
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(true)
                .setContentBackgroundResource(Color.argb(0,0,0,0))
                .create();

        View customView = dialogFont.getHolderView();

        LinearLayout f1 = customView.findViewById(R.id.f1);
        LinearLayout f2 = customView.findViewById(R.id.f2);
        LinearLayout f3 = customView.findViewById(R.id.f3);
        LinearLayout f4 = customView.findViewById(R.id.f4);
        LinearLayout f5 = customView.findViewById(R.id.f5);
        LinearLayout f6 = customView.findViewById(R.id.f6);
        LinearLayout f7 = customView.findViewById(R.id.f7);
        LinearLayout f8 = customView.findViewById(R.id.f8);
        LinearLayout f9 = customView.findViewById(R.id.f9);
        LinearLayout f10 = customView.findViewById(R.id.f10);

        TextView t1 = customView.findViewById(R.id.t1);
        TextView t2 = customView.findViewById(R.id.t2);
        TextView t3 = customView.findViewById(R.id.t3);
        TextView t4 = customView.findViewById(R.id.t4);
        TextView t5 = customView.findViewById(R.id.t5);
        TextView t6 = customView.findViewById(R.id.t6);
        TextView t7 = customView.findViewById(R.id.t7);
        TextView t8 = customView.findViewById(R.id.t8);
        TextView t9 = customView.findViewById(R.id.t9);
        TextView t10 = customView.findViewById(R.id.t10);

        t1.setTypeface(type1);
        t2.setTypeface(type2);
        t3.setTypeface(type3);
        t4.setTypeface(type4);
        t5.setTypeface(type5);
        t6.setTypeface(type6);
        t7.setTypeface(type7);
        t8.setTypeface(type8);
        t9.setTypeface(type9);
        t10.setTypeface(type10);

        f1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFont.dismiss();
                text.setTypeface(type1);
                selectedFont=1;
                text.setText("Barika");
            }
        });

        f2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFont.dismiss();
                text.setTypeface(type2);
                selectedFont=2;
                text.setText("Caviar Dreams");
            }
        });

        f3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFont.dismiss();
                text.setTypeface(type3);
                selectedFont=3;
                text.setText("Coolvetica");
            }
        });

        f4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFont.dismiss();
                text.setTypeface(type4);
                selectedFont=4;
                text.setText("Humble Boys");
            }
        });

        f5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFont.dismiss();
                text.setTypeface(type5);
                selectedFont=5;
                text.setText("Keep On Truckin");
            }
        });

        f6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFont.dismiss();
                text.setTypeface(type6);
                selectedFont=6;
                text.setText("Milky Nice");
            }
        });

        f7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFont.dismiss();
                text.setTypeface(type7);
                selectedFont=7;
                text.setText("Roboto");
            }
        });

        f8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFont.dismiss();
                text.setTypeface(type8);
                selectedFont=8;
                text.setText("Secret Admire");
            }
        });

        f9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFont.dismiss();
                text.setTypeface(type9);
                selectedFont=9;
                text.setText("Sketch 3D");
            }
        });

        f10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFont.dismiss();
                text.setTypeface(type10);
                selectedFont=10;
                text.setText("Vogue");
            }
        });

        dialogFont.show();

    }
}