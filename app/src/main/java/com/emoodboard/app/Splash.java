package com.emoodboard.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        Handler handler =  new Handler();
        Runnable myRunnable = new Runnable() {
            public void run() {
                // do something
                finish();
                Intent intent = new Intent(Splash.this,EditImageActivity.class);
                startActivity(intent);

            }
        };
        handler.postDelayed(myRunnable,2000);
    }
}