package com.emoodboard.app;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.ChangeBounds;
import androidx.transition.TransitionManager;

import com.emoodboard.app.base.BaseActivity;
import com.emoodboard.app.eraser.EraserActivity;
import com.emoodboard.app.filters.FilterListener;
import com.emoodboard.app.filters.FilterViewAdapter;
import com.emoodboard.app.picker.SelectPictureActivity;
import com.emoodboard.app.tools.EditingToolsAdapter;
import com.emoodboard.app.tools.ToolType;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
//import com.github.gabrielbb.cutout.CutOut;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
//import com.xinlan.imageeditlibrary.picchooser.SelectPictureActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import ja.burhanrashid52.photoeditor.OnPhotoEditorListener;
import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;
import ja.burhanrashid52.photoeditor.PhotoFilter;
import ja.burhanrashid52.photoeditor.SaveSettings;
import ja.burhanrashid52.photoeditor.TextStyleBuilder;
import ja.burhanrashid52.photoeditor.ViewType;

import static android.content.ClipDescription.MIMETYPE_TEXT_PLAIN;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class EditImageActivity extends BaseActivity implements OnPhotoEditorListener,
        View.OnClickListener,
        PropertiesBSFragment.Properties,
        EmojiBSFragment.EmojiListener,
        StickerBSFragment.StickerListener, EditingToolsAdapter.OnItemSelected, FilterListener {

    private static final String TAG = EditImageActivity.class.getSimpleName();
    public static final String FILE_PROVIDER_AUTHORITY = "com.burhanrashid52.photoeditor.fileprovider";
    private static final int CAMERA_REQUEST = 52;
    private static final int PICK_REQUEST = 53;
    private static final int ERASER_REQUEST = 54;
    PhotoEditor mPhotoEditor;
    private PhotoEditorView mPhotoEditorView;
    private PropertiesBSFragment mPropertiesBSFragment;
    private EmojiBSFragment mEmojiBSFragment;
    private StickerBSFragment mStickerBSFragment;
    private TextView mTxtCurrentTool;
    private Typeface mWonderFont;
    private RecyclerView mRvTools;
    private EditingToolsAdapter mEditingToolsAdapter = new EditingToolsAdapter(this);
    private FilterViewAdapter mFilterViewAdapter = new FilterViewAdapter(this, EditImageActivity.this);
    private BackgroundAdapter backgroundAdapter = new BackgroundAdapter(EditImageActivity.this);
    private FrameAdapter frameAdapter = new FrameAdapter(EditImageActivity.this);
    private ConstraintLayout mRootView;
    private ConstraintSet mConstraintSet = new ConstraintSet();
    private boolean mIsFilterVisible;

    @Nullable
    @VisibleForTesting
    Uri mSaveImageUri;

    LinearLayout layStart;
    Boolean isMenuShow=false;
    ImageView imgMenu;

    DialogPlus dialogMenu, dialogFilter, dialogBgImage, dialogFrame;

    public static final int REQUEST_PERMISSON_SORAGE = 1;
    public static final int SELECT_GALLERY_IMAGE_CODE = 7;

    Bitmap selectedBackgrundBitmap, selectedFrameBitmap;
    ArrayList<Uri> imageUris = new ArrayList<>();

    Typeface type1, type2, type3, type4, type5, type6, type7, type8, type9, type10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        makeFullScreen();
        setContentView(R.layout.activity_edit_image);

        initViews();

        handleIntentImage(mPhotoEditorView.getSource());

        mWonderFont = Typeface.createFromAsset(getAssets(), "beyond_wonderland.ttf");

        mPropertiesBSFragment = new PropertiesBSFragment();
        mEmojiBSFragment = new EmojiBSFragment();
        mStickerBSFragment = new StickerBSFragment();
        mStickerBSFragment.setStickerListener(this);
        mEmojiBSFragment.setEmojiListener(this);
        mPropertiesBSFragment.setPropertiesChangeListener(this);

        type1 = Typeface.createFromAsset(getAssets(), "f1_barika.ttf");
        type2 = Typeface.createFromAsset(getAssets(), "f2_caviar_dreams.ttf");
        type3 = Typeface.createFromAsset(getAssets(), "f3_coolvetica.ttf");
        type4 = Typeface.createFromAsset(getAssets(), "f4_humble_boys.otf");
        type5 = Typeface.createFromAsset(getAssets(), "f5_keepon_truckin.ttf");
        type6 = Typeface.createFromAsset(getAssets(), "f6_milky_nice.ttf");
        type7 = Typeface.createFromAsset(getAssets(), "f7_roboto.ttf");
        type8 = Typeface.createFromAsset(getAssets(), "f8_secret_admire.ttf");
        type9 = Typeface.createFromAsset(getAssets(), "f9_sketch_3d.otf");
        type10 = Typeface.createFromAsset(getAssets(), "f10_vogue.ttf");

        LinearLayoutManager llmTools = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRvTools.setLayoutManager(llmTools);
        mRvTools.setAdapter(mEditingToolsAdapter);


        //Typeface mTextRobotoTf = ResourcesCompat.getFont(this, R.font.roboto_medium);
        //Typeface mEmojiTypeFace = Typeface.createFromAsset(getAssets(), "emojione-android.ttf");

        mPhotoEditor = new PhotoEditor.Builder(this, mPhotoEditorView)
                .setPinchTextScalable(true) // set flag to make text scalable when pinch
                //.setDefaultTextTypeface(mTextRobotoTf)
                //.setDefaultEmojiTypeface(mEmojiTypeFace)
                .build(); // build photo editor sdk

        mPhotoEditor.setOnPhotoEditorListener(this);

        //Set Image Dynamically
        // mPhotoEditorView.getSource().setImageResource(R.drawable.color_palette);

        mPhotoEditorView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mPhotoEditorView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        Bitmap bitmapNew = Bitmap.createBitmap(mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), Bitmap.Config.ARGB_8888);
                        bitmapNew.eraseColor(Color.parseColor("#FFFFFF"));
                        mPhotoEditorView.getSource().setImageBitmap(bitmapNew);
                    }
                });



    }

    private void handleIntentImage(ImageView source) {
        Intent intent = getIntent();
        if (intent != null) {
            String intentType = intent.getType();
            if (intentType != null && intentType.startsWith("image/")) {
                Uri imageUri = intent.getData();
                if (imageUri != null) {
                    source.setImageURI(imageUri);
                }
            }
        }
    }

    private void initViews() {
        ImageView imgUndo;
        ImageView imgRedo;
        ImageView imgCamera;
        ImageView imgGallery;
        ImageView imgSave;
        ImageView imgClose;
        ImageView imgShare;

        layStart = findViewById(R.id.layStart);

        layStart.setOnClickListener(this);

        mPhotoEditorView = findViewById(R.id.photoEditorView);
        mTxtCurrentTool = findViewById(R.id.txtCurrentTool);
        mRvTools = findViewById(R.id.rvConstraintTools);
        mRootView = findViewById(R.id.rootView);

        imgUndo = findViewById(R.id.imgUndo);
        imgUndo.setOnClickListener(this);

        imgMenu = findViewById(R.id.imgMenu);
        imgMenu.setOnClickListener(this);

        imgRedo = findViewById(R.id.imgRedo);
        imgRedo.setOnClickListener(this);

        imgCamera = findViewById(R.id.imgCamera);
        imgCamera.setOnClickListener(this);

        imgGallery = findViewById(R.id.imgGallery);
        imgGallery.setOnClickListener(this);

        imgSave = findViewById(R.id.imgSave);
        imgSave.setOnClickListener(this);

        imgClose = findViewById(R.id.imgClose);
        imgClose.setOnClickListener(this);

        imgShare = findViewById(R.id.imgShare);
        imgShare.setOnClickListener(this);

        initDialogMenu();

        ViewTreeObserver vto = mPhotoEditorView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    mPhotoEditorView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    mPhotoEditorView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }

                Bitmap bitmapNew = Bitmap.createBitmap(mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), Bitmap.Config.ARGB_8888);
                bitmapNew.eraseColor(getResources().getColor(R.color.white));
                selectedBackgrundBitmap = bitmapNew;

                Bitmap bitmapN = BitmapFactory.decodeResource(getResources(), R.drawable.frame_0);
                Bitmap bitmapN2 = Bitmap.createScaledBitmap(bitmapN, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
                selectedFrameBitmap = bitmapN2;

            }
        });


    }

    @Override
    public void onEditTextChangeListener(final View rootView, String text, int colorCode) {
        TextEditorDialogFragment textEditorDialogFragment =
                TextEditorDialogFragment.show(this, text, colorCode);
        textEditorDialogFragment.setOnTextEditorListener(new TextEditorDialogFragment.TextEditor() {
            @Override
            public void onDone(String inputText, int colorCode) {
                final TextStyleBuilder styleBuilder = new TextStyleBuilder();
                styleBuilder.withTextColor(colorCode);

                mPhotoEditor.editText(rootView, inputText, styleBuilder);
                mTxtCurrentTool.setText(R.string.label_text);
            }
        });
    }

    @Override
    public void onAddViewListener(ViewType viewType, int numberOfAddedViews) {
        Log.d(TAG, "onAddViewListener() called with: viewType = [" + viewType + "], numberOfAddedViews = [" + numberOfAddedViews + "]");
    }

    @Override
    public void onRemoveViewListener(ViewType viewType, int numberOfAddedViews) {
        Log.d(TAG, "onRemoveViewListener() called with: viewType = [" + viewType + "], numberOfAddedViews = [" + numberOfAddedViews + "]");
    }

    @Override
    public void onStartViewChangeListener(ViewType viewType) {
        Log.d(TAG, "onStartViewChangeListener() called with: viewType = [" + viewType + "]");
    }

    @Override
    public void onStopViewChangeListener(ViewType viewType) {
        Log.d(TAG, "onStopViewChangeListener() called with: viewType = [" + viewType + "]");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imgMenu:
                showDidalogMenu();
                break;

            case R.id.imgUndo:
                mPhotoEditor.undo();
                break;

            case R.id.imgRedo:
                mPhotoEditor.redo();
                break;

            case R.id.imgCamera:
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                break;
        }
    }


    private void shareImage() {
        if (mSaveImageUri == null) {
            showSnackbar(getString(R.string.msg_save_image_to_share));
            return;
        }
        imageUris.clear();
        imageUris.add(mSaveImageUri);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND_MULTIPLE);
        intent.setType("text/plain");
        intent.setType("image/jpeg");
        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    private Uri buildFileProviderUri(@NonNull Uri uri) {
        return FileProvider.getUriForFile(this,
                FILE_PROVIDER_AUTHORITY,
                new File(uri.getPath()));
    }

    @SuppressLint("MissingPermission")
    private void saveImage(final Boolean isSharing) {
        if (requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            showLoading("Please Wait...");
            File dir = new File(Environment.getExternalStorageDirectory(), "E-Moodboard");
            try{
                if(dir.mkdir()) {
                    System.out.println("Directory created");
                } else {
                    System.out.println("Directory is not created");
                }
            }catch(Exception e){
                e.printStackTrace();
            }

            File file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "E-Moodboard" + File.separator + ""
                    + System.currentTimeMillis() + ".jpg");
            try {
                file.createNewFile();

                SaveSettings saveSettings = new SaveSettings.Builder()
                        .setClearViewsEnabled(false)
                        .setTransparencyEnabled(true)
                        .build();

                mPhotoEditor.saveAsFile(file.getAbsolutePath(), saveSettings, new PhotoEditor.OnSaveListener() {
                    @Override
                    public void onSuccess(@NonNull String imagePath) {
                        if (isSharing)
                        {
                            hideLoading();
                            mSaveImageUri = Uri.fromFile(new File(imagePath));
                            shareImage();
                        }
                        else
                        {
                            hideLoading();
                            showSnackbar("Gambar disimpan di folder E-Moodboard");
                            mSaveImageUri = Uri.fromFile(new File(imagePath));
                            //mPhotoEditorView.getSource().setImageURI(mSaveImageUri);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        hideLoading();
                        showSnackbar("Gagal menyimpan gambar");
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
                hideLoading();
                showSnackbar(e.getMessage());
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (resultCode == RESULT_OK) {
                        Uri uriImage = result.getUri();
                        Intent intent2 = new Intent(EditImageActivity.this, EraserActivity.class);
                        intent2.putExtra("path", ""+uriImage.getPath());
                        intent2.putExtra("type", "gallery");
                        startActivityForResult(intent2, ERASER_REQUEST);
                    }
                    break;

                case CAMERA_REQUEST:
                    //mPhotoEditor.clearAllViews();
                    Bitmap photo = (Bitmap) data.getExtras().get("data");

//                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                    photo.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
//                    String path = MediaStore.Images.Media.insertImage(getApplicationContext().getContentResolver(), photo, "img_" + System.currentTimeMillis() + ".png", null);

                    //Convert to byte array
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] byteArray = stream.toByteArray();

//                    Intent in1 = new Intent(this, Activity2.class);
//                    in1.putExtra("image",byteArray);

                    Intent intent = new Intent(EditImageActivity.this, EraserActivity.class);
                    intent.putExtra("path", byteArray);
                    intent.putExtra("type", "camera");
                    startActivityForResult(intent, ERASER_REQUEST);

                    //mPhotoEditor.addImage(photo);
//                    CutOut.activity()
//                            .src(Uri.parse(path))
//                            .start(this);
                    break;
                case PICK_REQUEST:
                    String filepath = data.getStringExtra("imgPath");
                    Intent intent2 = new Intent(EditImageActivity.this, EraserActivity.class);
                    intent2.putExtra("path", filepath);
                    intent2.putExtra("type", "gallery");
                    startActivityForResult(intent2, ERASER_REQUEST);
                    //Bitmap bitmap = BitmapFactory.decodeFile(filepath);
                    //mPhotoEditor.addImage(bitmap);

//                    CutOut.activity()
//                            .src(Uri.parse(filepath))
//                            .start(this);
                    break;

//                case CutOut.CUTOUT_ACTIVITY_REQUEST_CODE:
//                    Uri imageUri = CutOut.getUri(data);
//                    try {
//                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
//                        mPhotoEditor.addImage(bitmap);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    break;

                case 888:
                    int colorCode = data.getIntExtra("color", -16777216);
                    int selectedFont = data.getIntExtra("font", 7);
                    String myText = ""+data.getStringExtra("text");
                    //Toast.makeText(getApplicationContext(), ""+colorCode, Toast.LENGTH_SHORT).show();

                    final TextStyleBuilder styleBuilder = new TextStyleBuilder();
                    styleBuilder.withTextColor(colorCode);

                    if(selectedFont == 1)
                    {
                        styleBuilder.withTextFont(type1);
                    }
                    else if(selectedFont == 2)
                    {
                        styleBuilder.withTextFont(type2);
                    }
                    else if(selectedFont == 3)
                    {
                        styleBuilder.withTextFont(type3);
                    }
                    else if(selectedFont == 4)
                    {
                        styleBuilder.withTextFont(type4);
                    }
                    else if(selectedFont == 5)
                    {
                        styleBuilder.withTextFont(type5);
                    }
                    else if(selectedFont == 6)
                    {
                        styleBuilder.withTextFont(type6);
                    }
                    else if(selectedFont == 7)
                    {
                        styleBuilder.withTextFont(type7);
                    }
                    else if(selectedFont == 8)
                    {
                        styleBuilder.withTextFont(type8);
                    }
                    else if(selectedFont == 9)
                    {
                        styleBuilder.withTextFont(type9);
                    }
                    else if(selectedFont == 10)
                    {
                        styleBuilder.withTextFont(type10);
                    }
                    else
                    {
                        styleBuilder.withTextFont(type1);
                    }

                    mPhotoEditor.addText(myText, styleBuilder);
                    break;

                case ERASER_REQUEST:
                    String myPath = ""+data.getStringExtra("path");
                    Bitmap bitmap = BitmapFactory.decodeFile(myPath);
                    Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth()/2, bitmap.getHeight()/2, false);
                    mPhotoEditor.addImage(bitmap2);
                    break;
            }
        }
    }

    @Override
    public void onColorChanged(int colorCode) {
        mPhotoEditor.setBrushColor(colorCode);
        mTxtCurrentTool.setText(R.string.label_brush);
    }

    @Override
    public void onOpacityChanged(int opacity) {
        mPhotoEditor.setOpacity(opacity);
        mTxtCurrentTool.setText(R.string.label_brush);
    }

    @Override
    public void onBrushSizeChanged(int brushSize) {
        mPhotoEditor.setBrushSize(brushSize);
        mTxtCurrentTool.setText(R.string.label_brush);
    }

    @Override
    public void onEmojiClick(String emojiUnicode) {
        mPhotoEditor.addEmoji(emojiUnicode);
        mTxtCurrentTool.setText(R.string.label_emoji);
    }

    @Override
    public void onStickerClick(Bitmap bitmap) {
        Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth()/2, bitmap.getHeight()/2, false);
        mPhotoEditor.addImage(bitmap2);
        mTxtCurrentTool.setText(R.string.label_sticker);
    }

    @Override
    public void isPermissionGranted(boolean isGranted, String permission) {
        if (isGranted) {
            saveImage(false);
        }
    }

    private void showSaveDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.msg_save_image));
        builder.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveImage(false);
            }
        });
        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("Keluar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.create().show();

    }

    @Override
    public void onFilterSelected(PhotoFilter photoFilter) {
        mPhotoEditor.setFilterEffect(photoFilter);
    }

    @Override
    public void onToolSelected(ToolType toolType) {
        switch (toolType) {
            case BRUSH:

                break;
            case TEXT:

                break;
            case ERASER:
                mPhotoEditor.brushEraser();
                mTxtCurrentTool.setText(R.string.label_eraser_mode);
                break;
            case FILTER:
                mTxtCurrentTool.setText(R.string.label_filter);
                showFilter(true);
                break;
            case EMOJI:
                break;
            case STICKER:

                break;
        }
    }


    void showFilter(boolean isVisible) {
        mIsFilterVisible = isVisible;
        mConstraintSet.clone(mRootView);

//        if (isVisible) {
//            mConstraintSet.clear(mRvFilters.getId(), ConstraintSet.START);
//            mConstraintSet.connect(mRvFilters.getId(), ConstraintSet.START,
//                    ConstraintSet.PARENT_ID, ConstraintSet.START);
//            mConstraintSet.connect(mRvFilters.getId(), ConstraintSet.END,
//                    ConstraintSet.PARENT_ID, ConstraintSet.END);
//        } else {
//            mConstraintSet.connect(mRvFilters.getId(), ConstraintSet.START,
//                    ConstraintSet.PARENT_ID, ConstraintSet.END);
//            mConstraintSet.clear(mRvFilters.getId(), ConstraintSet.END);
//        }
//
//        ChangeBounds changeBounds = new ChangeBounds();
//        changeBounds.setDuration(350);
//        changeBounds.setInterpolator(new AnticipateOvershootInterpolator(1.0f));
//        TransitionManager.beginDelayedTransition(mRootView, changeBounds);
//
//        mConstraintSet.applyTo(mRootView);
    }

    @Override
    public void onBackPressed() {
        if (mIsFilterVisible) {
            showFilter(false);
            mTxtCurrentTool.setText(R.string.app_name);
        } else if (!mPhotoEditor.isCacheEmpty()) {
            showSaveDialog();
        } else {
            super.onBackPressed();
        }
//        if (mIsFilterVisible) {
//            showFilter(false);
//            mTxtCurrentTool.setText(R.string.app_name);
//        }else {
//            showSaveDialog();
//        }
    }

//    public void showHideMenu()
//    {
//        if(isMenuShow)
//        {
//            imgMenu.setImageResource(R.drawable.ic_baseline_white);
//            slideDown();
//        }
//        else
//        {
//            imgMenu.setImageResource(R.drawable.ic_arrow_down_white);
//            slideUp();
//        }
//        isMenuShow = !isMenuShow;
//    }

//    public void slideUp(){
//        TranslateAnimation animate = new TranslateAnimation(
//                0,
//                0,
//                layAdd.getHeight(),
//                0
//                );
//        animate.setDuration(300);
//        animate.setFillAfter(true);
//        layAdd.startAnimation(animate);
//
//    }
//
//    public void slideDownFast(){
//        //Toast.makeText(getApplicationContext(),"WOY",Toast.LENGTH_SHORT).show();
//        TranslateAnimation animate = new TranslateAnimation(
//                0,
//                0,
//                0,
//                layAdd.getHeight());
//        animate.setDuration(1);
//        animate.setFillAfter(true);
//        layAdd.startAnimation(animate);
//
//    }
//
//    public void slideDown() {
//        TranslateAnimation animate = new TranslateAnimation(
//                0,
//                0,
//                0,
//                layAdd.getHeight());
//        animate.setDuration(300);
//        animate.setFillAfter(true);
//        layAdd.startAnimation(animate);
//    }


    public void selectBackgroundColor() {
        ColorPickerDialogBuilder
                .with(this)
                .setTitle("Pilih Warna Background")
                .initialColor(Color.WHITE)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setOnColorSelectedListener(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int selectedColor) {
                        //Toast.makeText(getApplicationContext(), "onColorSelected: 0x" + Integer.toHexString(selectedColor), Toast.LENGTH_SHORT).show();
                    }
                })
                .setPositiveButton("Pilih", new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        mPhotoEditorView.setBackgroundColor(selectedColor);
                        mRootView.setBackgroundColor(selectedColor);
                        Bitmap bitmapNew = Bitmap.createBitmap(mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), Bitmap.Config.ARGB_8888);
                        bitmapNew.eraseColor(selectedColor);
                        selectedBackgrundBitmap = bitmapNew;

                        Bitmap finalBmp = Bitmap.createBitmap(mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), Bitmap.Config.ARGB_8888);
                        Canvas canvas = new Canvas(finalBmp);
                        canvas.drawBitmap(bitmapNew, 0, 0, null);
                        canvas.drawBitmap(selectedFrameBitmap, 0, 0, null);
                        mPhotoEditorView.getSource().setImageBitmap(finalBmp);
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .build()
                .show();
    }



    public void initDialogMenu() {
        dialogMenu = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.model_dialog_menu))
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(true)
                .setContentBackgroundResource(Color.argb(0,0,0,0))
                .create();

        View customView = dialogMenu.getHolderView();

        LinearLayout menuBackground = customView.findViewById(R.id.menuBackground);
        LinearLayout menuFrame = customView.findViewById(R.id.menuFrame);
        LinearLayout menuText = customView.findViewById(R.id.menuText);
        LinearLayout menuFabric = customView.findViewById(R.id.menuFabric);
        LinearLayout menuImage = customView.findViewById(R.id.menuImage);
        LinearLayout menuPallete = customView.findViewById(R.id.menuPallete);
        LinearLayout menuLink = customView.findViewById(R.id.menuLink);
        //LinearLayout menuCutting = customView.findViewById(R.id.menuCutting);
        LinearLayout menuEmoji = customView.findViewById(R.id.menuEmoji);
        LinearLayout menuShare = customView.findViewById(R.id.menuShare);
        LinearLayout menuSave = customView.findViewById(R.id.menuSave);
        LinearLayout menuKeluar = customView.findViewById(R.id.menuKeluar);
        ImageView hide = customView.findViewById(R.id.hide);


        menuBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMenu.dismiss();
                Handler handler =  new Handler();
                Runnable myRunnable = new Runnable() {
                    public void run() {
                        //showFilter(true);
                        dialogBackground();
                    }
                };
                handler.postDelayed(myRunnable,500);
            }
        });

        menuFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMenu.dismiss();
                //Toast.makeText(getApplicationContext(), "Frame", Toast.LENGTH_SHORT).show();
                Handler handler =  new Handler();
                Runnable myRunnable = new Runnable() {
                    public void run() {
                        //showFilter(true);
                        dialogFrame();
                    }
                };
                handler.postDelayed(myRunnable,500);
            }
        });

        menuText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMenu.dismiss();
                //showMenuText();
                Intent intent = new Intent(EditImageActivity.this, AddText.class);
                startActivityForResult(intent, 888);
            }
        });

        menuFabric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMenu.dismiss();
                //Toast.makeText(getApplicationContext(), "Fabric belum Tersedia", Toast.LENGTH_SHORT).show();
                mStickerBSFragment.show(getSupportFragmentManager(), mStickerBSFragment.getTag());
            }
        });

        menuImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMenu.dismiss();
                Handler handler =  new Handler();
                Runnable myRunnable = new Runnable() {
                    public void run() {
                        //showFilter(true);
                        dialogImage();
                    }
                };
                handler.postDelayed(myRunnable,500);

//                Intent intent = new Intent();
//                intent.setType("image/*");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(Intent.createChooser(intent, "Pilih Gambar"), PICK_REQUEST);
            }
        });

        menuPallete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMenu.dismiss();
                Handler handler =  new Handler();
                Runnable myRunnable = new Runnable() {
                    public void run() {
                        dialogBrush();
                    }
                };
                handler.postDelayed(myRunnable,500);

            }
        });

        menuLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMenu.dismiss();
                //Toast.makeText(getApplicationContext(), "Link belum Tersedia", Toast.LENGTH_SHORT).show();
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                String pasteData = "";

                if (!(clipboard.hasPrimaryClip())) {
                     Toast.makeText(getApplicationContext(), "Link belum dicopy", Toast.LENGTH_SHORT).show();

                } else if (!(clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN))) {

                    Toast.makeText(getApplicationContext(), "Link belum dicopy", Toast.LENGTH_SHORT).show();

                } else {

                    //since the clipboard contains plain text.
                    ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);

                    // Gets the clipboard as text.
                    pasteData = item.getText().toString();
                }
                Typeface myTypeface = Typeface.createFromAsset(getAssets(), "font_link.ttf");
                TextStyleBuilder styleBuilder = new TextStyleBuilder();
                styleBuilder.withTextColor(getResources().getColor(R.color.blue_color_picker));
                styleBuilder.withTextFont(myTypeface);
                mPhotoEditor.addText(pasteData, styleBuilder);
            }
        });

//        menuCutting.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //Toast.makeText(getApplicationContext(), "Cutting belum Tersedia", Toast.LENGTH_SHORT).show();
////                Intent intent = new Intent(EditImageActivity.this, EraserActivity.class);
////                startActivity(intent);
//            }
//        });

        menuEmoji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mEmojiBSFragment.show(getSupportFragmentManager(), mEmojiBSFragment.getTag());
                dialogMenu.dismiss();
                Handler handler =  new Handler();
                Runnable myRunnable = new Runnable() {
                    public void run() {
                        //showFilter(true);
                        dialogFilter();
                    }
                };
                handler.postDelayed(myRunnable,500);


            }
        });


        menuShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMenu.dismiss();
                saveImage(true);
            }
        });

        menuSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMenu.dismiss();
                saveImage(false);
            }
        });

        menuKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMenu.dismiss();
                onBackPressed();
            }
        });

        hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMenu.dismiss();
            }
        });


    }

    public void dialogBrush() {
        final DialogPlus dialogBrush = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.model_dialog_brush))
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(true)
                .setContentBackgroundResource(Color.argb(0,0,0,0))
                .create();

        View customView = dialogBrush.getHolderView();

        LinearLayout menuBrush = customView.findViewById(R.id.menuBrush);
        LinearLayout menuEraser = customView.findViewById(R.id.menuEraser);
        ImageView hide = customView.findViewById(R.id.hide);


        menuBrush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBrush.dismiss();
                mPhotoEditor.setBrushDrawingMode(true);
                mTxtCurrentTool.setText(R.string.label_brush);
                mPropertiesBSFragment.show(getSupportFragmentManager(), mPropertiesBSFragment.getTag());
            }
        });

        menuEraser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBrush.dismiss();
                mPhotoEditor.brushEraser();
                mTxtCurrentTool.setText(R.string.label_eraser_mode);
            }
        });



        hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBrush.dismiss();
            }
        });

        dialogBrush.show();

    }


    public void dialogBackground() {
        //Toast.makeText(getApplicationContext(), "Masuk Dialog Bg", Toast.LENGTH_SHORT).show();
        final DialogPlus dialogBg = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.model_dialog_background))
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(true)
                .setContentBackgroundResource(Color.argb(0,0,0,0))
                .create();

        View customView = dialogBg.getHolderView();

        LinearLayout menuColor = customView.findViewById(R.id.menuColor);
        LinearLayout menuImage = customView.findViewById(R.id.menuImage);
        ImageView hide = customView.findViewById(R.id.hide);


        menuColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBg.dismiss();
                selectBackgroundColor();
            }
        });

        menuImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBg.dismiss();
                Handler handler =  new Handler();
                Runnable myRunnable = new Runnable() {
                    public void run() {
                        //showFilter(true);
                        dialogBgImage();
                    }
                };
                handler.postDelayed(myRunnable,500);

            }
        });



        hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBg.dismiss();
            }
        });

        dialogBg.show();

    }


    public void dialogImage() {
        //Toast.makeText(getApplicationContext(), "Masuk Dialog Bg", Toast.LENGTH_SHORT).show();
//        final DialogPlus dialogImg = DialogPlus.newDialog(this)
//                .setContentHolder(new ViewHolder(R.layout.model_dialog_image))
//                .setInAnimation(R.anim.slide_in_bottom)
//                .setOutAnimation(R.anim.slide_out_bottom)
//                .setCancelable(true)
//                .setContentBackgroundResource(Color.argb(0,0,0,0))
//                .create();
//
//        View customView = dialogImg.getHolderView();
//
//        LinearLayout menuCam = customView.findViewById(R.id.menuCam);
//        LinearLayout menuGal = customView.findViewById(R.id.menuGal);
//        ImageView hide = customView.findViewById(R.id.hide);
//
//
//        menuCam.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialogImg.dismiss();
//                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(cameraIntent, CAMERA_REQUEST);
//            }
//        });
//
//        menuGal.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialogImg.dismiss();
//                selectFromAblum();
//            }
//        });
//
//
//
//        hide.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialogImg.dismiss();
//            }
//        });
//
//        dialogImg.show();

        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(false)
                .start(this);

    }


    public void dialogBgImage() {
        //Toast.makeText(getApplicationContext(), "Masuk Dialog Bg Image", Toast.LENGTH_SHORT).show();
        dialogBgImage = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.model_dialog_filter))
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(true)
                .setContentBackgroundResource(Color.argb(0,0,0,0))
                .create();

        View customView = dialogBgImage.getHolderView();

        ImageView hide = customView.findViewById(R.id.hide);
        RecyclerView mRvFilters = customView.findViewById(R.id.rvFilterView);

        LinearLayoutManager llmFilters = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRvFilters.setLayoutManager(llmFilters);
        mRvFilters.setAdapter(backgroundAdapter);

        hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBgImage.dismiss();
            }
        });

        dialogBgImage.show();

    }


    public void dialogFrame() {
        //Toast.makeText(getApplicationContext(), "Masuk Dialog Frame", Toast.LENGTH_SHORT).show();
        dialogFrame = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.model_dialog_filter))
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(true)
                .setContentBackgroundResource(Color.argb(0,0,0,0))
                .create();

        View customView = dialogFrame.getHolderView();

        ImageView hide = customView.findViewById(R.id.hide);
        RecyclerView mRvFilters = customView.findViewById(R.id.rvFilterView);

        LinearLayoutManager llmFilters = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRvFilters.setLayoutManager(llmFilters);
        mRvFilters.setAdapter(frameAdapter);

        hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFrame.dismiss();
            }
        });

        dialogFrame.show();

    }

    public void dialogFilter() {
        dialogFilter = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.model_dialog_filter))
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(true)
                .setContentBackgroundResource(Color.argb(0,0,0,0))
                .create();

        View customView = dialogFilter.getHolderView();

        ImageView hide = customView.findViewById(R.id.hide);
        RecyclerView mRvFilters = customView.findViewById(R.id.rvFilterView);

        LinearLayoutManager llmFilters = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRvFilters.setLayoutManager(llmFilters);
        mRvFilters.setAdapter(mFilterViewAdapter);

        hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFilter.dismiss();
            }
        });

        dialogFilter.show();

    }


    public void onBgSelected(int position)
    {
        dialogBgImage.dismiss();
        if(position == 0)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.bg_1);
            selectedBackgrundBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }
        else if(position == 1)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.bg_2);
            selectedBackgrundBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }
        else if(position == 2)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.bg_3);
            selectedBackgrundBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }
        else if(position == 3)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.bg_4);
            selectedBackgrundBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }
        else if(position == 4)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.bg_5);
            selectedBackgrundBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }
        else if(position == 5)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.bg_6);
            selectedBackgrundBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }
        else if(position == 6)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.bg_7);
            selectedBackgrundBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }
        else if(position == 7)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.bg_8);
            selectedBackgrundBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }
        else if(position == 8)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.bg_9);
            selectedBackgrundBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }
        else if(position == 9)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.bg_10);
            selectedBackgrundBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }

        Bitmap finalBmp = Bitmap.createBitmap(mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(finalBmp);
        canvas.drawBitmap(selectedBackgrundBitmap, 0, 0, null);
        canvas.drawBitmap(selectedFrameBitmap, 0, 0, null);
        mPhotoEditorView.getSource().setImageBitmap(finalBmp);
    }


    public void onFrameSelected(int position)
    {
        dialogFrame.dismiss();
        if(position == 0)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.frame_0);
            selectedFrameBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }
        else if(position == 1)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.frame_1);
            selectedFrameBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }
        else if(position == 2)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.frame_2);
            selectedFrameBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }
        else if(position == 3)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.frame_3);
            selectedFrameBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }
        else if(position == 4)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.frame_4);
            selectedFrameBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }
        else if(position == 5)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.frame_5);
            selectedFrameBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }
        else if(position == 6)
        {
            Bitmap bitmapNew = BitmapFactory.decodeResource(getResources(), R.drawable.frame_6);
            selectedFrameBitmap = Bitmap.createScaledBitmap(bitmapNew, mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), false);
        }

        Bitmap finalBmp = Bitmap.createBitmap(mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(finalBmp);
        canvas.drawBitmap(selectedBackgrundBitmap, 0, 0, null);
        canvas.drawBitmap(selectedFrameBitmap, 0, 0, null);
        mPhotoEditorView.getSource().setImageBitmap(finalBmp);
    }


    public void onFilterSelected(int position)
    {
        dialogFilter.dismiss();
        if(position == 0)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.NONE);
        }
        else if(position == 1)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.AUTO_FIX);
        }
        else if(position == 2)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.BRIGHTNESS);
        }
        else if(position == 3)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.CONTRAST);
        }
        else if(position == 4)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.DOCUMENTARY);
        }
        else if(position == 5)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.DUE_TONE);
        }
        else if(position == 6)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.FILL_LIGHT);
        }
        else if(position == 7)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.FISH_EYE);
        }
        else if(position == 8)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.GRAIN);
        }
        else if(position == 9)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.GRAY_SCALE);
        }
        else if(position == 10)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.LOMISH);
        }
        else if(position == 11)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.NEGATIVE);
        }
        else if(position == 12)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.POSTERIZE);
        }
        else if(position == 13)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.SATURATE);
        }
        else if(position == 14)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.SEPIA);
        }
        else if(position == 15)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.SHARPEN);
        }
        else if(position == 16)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.TEMPERATURE);
        }
        else if(position == 17)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.TINT);
        }
        else if(position == 18)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.VIGNETTE);
        }
        else if(position == 19)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.CROSS_PROCESS);
        }
        else if(position == 20)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.BLACK_WHITE);
        }
        else if(position == 21)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.FLIP_HORIZONTAL);
        }
        else if(position == 22)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.FLIP_VERTICAL);
        }
        else if(position == 23)
        {
            mPhotoEditor.setFilterEffect(PhotoFilter.ROTATE);
        }
    }

    public void showDidalogMenu()
    {
        dialogMenu.show();
    }

    public void showMenuText()
    {
        TextEditorDialogFragment textEditorDialogFragment = TextEditorDialogFragment.show(this);
        textEditorDialogFragment.setOnTextEditorListener(new TextEditorDialogFragment.TextEditor() {
            @Override
            public void onDone(String inputText, int colorCode) {
                final TextStyleBuilder styleBuilder = new TextStyleBuilder();
                styleBuilder.withTextColor(colorCode);

                mPhotoEditor.addText(inputText, styleBuilder);
                mTxtCurrentTool.setText(R.string.label_text);
            }
        });
    }

    private void selectFromAblum() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            openAblumWithPermissionsCheck();
        } else {
            openAblum();
        }//end if
    }

    private void openAblum() {
        EditImageActivity.this.startActivityForResult(new Intent(
                        EditImageActivity.this, SelectPictureActivity.class),
                PICK_REQUEST);
//        CutOut.activity()
//                .start(this);
    }

    private void openAblumWithPermissionsCheck() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_PERMISSON_SORAGE);
            return;
        }
        openAblum();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_PERMISSON_SORAGE
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openAblum();
            return;
        }//end if

//        if (requestCode == REQUEST_PERMISSON_CAMERA
//                && grantResults.length > 0
//                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            doTakePhoto();
//            return;
        }//end if

}
