package com.emoodboard.app;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.emoodboard.app.filters.FilterListener;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import ja.burhanrashid52.photoeditor.PhotoFilter;

public class BackgroundAdapter extends RecyclerView.Adapter<BackgroundAdapter.ViewHolder> {

    private List<Integer> mPairList = new ArrayList<>();
    private EditImageActivity mEditImageActivity;

    public BackgroundAdapter(EditImageActivity editImageActivity) {
        mEditImageActivity = editImageActivity;
        setupFilters();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_bg_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        int ur = position+1;
        holder.mImageFilterView.setImageResource(mPairList.get(position));
        holder.mTxtFilterName.setText("BG "+ur);
        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditImageActivity.onBgSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPairList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImageFilterView;
        TextView mTxtFilterName;
        LinearLayout parent;

        ViewHolder(View itemView) {
            super(itemView);
            mImageFilterView = itemView.findViewById(R.id.imgFilterView);
            mTxtFilterName = itemView.findViewById(R.id.txtFilterName);
            parent = itemView.findViewById(R.id.parent);
        }
    }

    private void setupFilters() {
        mPairList.add(R.drawable.bg_1);
        mPairList.add(R.drawable.bg_2);
        mPairList.add(R.drawable.bg_3);
        mPairList.add(R.drawable.bg_4);
        mPairList.add(R.drawable.bg_5);
        mPairList.add(R.drawable.bg_6);
        mPairList.add(R.drawable.bg_7);
        mPairList.add(R.drawable.bg_8);
        mPairList.add(R.drawable.bg_9);
        mPairList.add(R.drawable.bg_10);
    }
}
