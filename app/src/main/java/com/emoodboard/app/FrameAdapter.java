package com.emoodboard.app;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class FrameAdapter extends RecyclerView.Adapter<FrameAdapter.ViewHolder> {

    private List<Integer> mPairList = new ArrayList<>();
    private EditImageActivity mEditImageActivity;

    public FrameAdapter(EditImageActivity editImageActivity) {
        mEditImageActivity = editImageActivity;
        setupFilters();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_bg_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        int ur = position+1;
        holder.mImageFilterView.setImageResource(mPairList.get(position));
        if(position==0)
        {
            holder.mTxtFilterName.setText("None");
        }
        else
        {
            holder.mTxtFilterName.setText("Frame "+ur);
        }

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditImageActivity.onFrameSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPairList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImageFilterView;
        TextView mTxtFilterName;
        LinearLayout parent;

        ViewHolder(View itemView) {
            super(itemView);
            mImageFilterView = itemView.findViewById(R.id.imgFilterView);
            mTxtFilterName = itemView.findViewById(R.id.txtFilterName);
            parent = itemView.findViewById(R.id.parent);
        }
    }

    private void setupFilters() {
        mPairList.add(R.drawable.frame_0);
        mPairList.add(R.drawable.frame_1);
        mPairList.add(R.drawable.frame_2);
        mPairList.add(R.drawable.frame_3);
        mPairList.add(R.drawable.frame_4);
        mPairList.add(R.drawable.frame_5);
        mPairList.add(R.drawable.frame_6);
    }
}
