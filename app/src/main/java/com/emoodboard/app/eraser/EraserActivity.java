package com.emoodboard.app.eraser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.core.app.ActivityCompat;

import com.emoodboard.app.R;


public class EraserActivity extends Activity implements OnClickListener {

    private ContentResolver mContentResolver;
    private Bitmap mBitmap;

    HoverView mHoverView;
    double mDensity;

    int viewWidth;
    int viewHeight;
    int bmWidth;
    int bmHeight;

    int actionBarHeight;
    int bottombarHeight;
    double bmRatio;
    double viewRatio;

    ImageView eraserSubButton, unEraserSubButton, back;
    ImageView brushSizexButton, brushSize0Button, brushSize1Button, brushSize2Button, brushSize3Button, brushSize4Button;
    ImageView  undoButton, redoButton;
    Button nextButton;

    LinearLayout eraserLayout;
    RelativeLayout mLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eraser);
        mContentResolver = getContentResolver();

        if (Build.VERSION.SDK_INT >= 23 ) {
            if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }

        String type = "";
        try {
            type = ""+getIntent().getStringExtra("type");
        } catch (Exception e) {
            e.printStackTrace();
            type = "";
        }

        if(type.equals("camera"))
        {
            byte[] byteArray = getIntent().getByteArrayExtra("path");
            mBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        }
        else
        {
            String path = "";
            try {
                path = ""+getIntent().getStringExtra("path");
            } catch (Exception e) {
                e.printStackTrace();
                path ="";
            }
            //mBitmap =  BitmapFactory.decodeResource(getResources(),R.drawable.bg_1);
            mBitmap = BitmapFactory.decodeFile(path);
        }




        mLayout = (RelativeLayout) findViewById(R.id.mainLayout);
        mDensity = getResources().getDisplayMetrics().density;
        actionBarHeight = (int)(110*mDensity);
        bottombarHeight = (int)(60*mDensity);


        viewWidth = getResources().getDisplayMetrics().widthPixels;
        viewHeight = getResources().getDisplayMetrics().heightPixels - actionBarHeight - bottombarHeight;
        viewRatio = (double) viewHeight/ (double) viewWidth;

        bmRatio = (double) mBitmap.getHeight()/ (double) mBitmap.getWidth();
        if(bmRatio < viewRatio) {
            bmWidth = viewWidth;
            bmHeight = (int)(((double)viewWidth)*((double)(mBitmap.getHeight())/(double)(mBitmap.getWidth())));
        } else {
            bmHeight = viewHeight;
            bmWidth = (int)(((double)viewHeight)*((double)(mBitmap.getWidth())/(double)(mBitmap.getHeight())));
        }


        mBitmap = Bitmap.createScaledBitmap(mBitmap, bmWidth, bmHeight, false);

        mHoverView = new HoverView(this, mBitmap, bmWidth, bmHeight, viewWidth, viewHeight);
        mHoverView.setLayoutParams(new LayoutParams(viewWidth, viewHeight));

        mLayout.addView(mHoverView);

        initButton();

    }

    public void initButton() {

        eraserSubButton = (ImageView) findViewById(R.id.erase_sub_button);
        eraserSubButton.setOnClickListener(this);

        unEraserSubButton = (ImageView) findViewById(R.id.unerase_sub_button);
        unEraserSubButton.setOnClickListener(this);

        brushSizexButton = (ImageView) findViewById(R.id.brush_size_x_button);
        brushSizexButton.setOnClickListener(this);

        brushSize0Button = (ImageView) findViewById(R.id.brush_size_0_button);
        brushSize0Button.setOnClickListener(this);

        brushSize1Button = (ImageView) findViewById(R.id.brush_size_1_button);
        brushSize1Button.setOnClickListener(this);

        brushSize2Button = (ImageView) findViewById(R.id.brush_size_2_button);
        brushSize2Button.setOnClickListener(this);

        brushSize3Button = (ImageView) findViewById(R.id.brush_size_3_button);
        brushSize3Button.setOnClickListener(this);

        brushSize4Button = (ImageView) findViewById(R.id.brush_size_4_button);
        brushSize4Button.setOnClickListener(this);


        nextButton = (Button) findViewById(R.id.nextButton);
        nextButton.setOnClickListener(this);

        undoButton = (ImageView) findViewById(R.id.undoButton);
        undoButton.setOnClickListener(this);

        redoButton = (ImageView) findViewById(R.id.redoButton);
        redoButton.setOnClickListener(this);
        updateRedoButton();

        eraserLayout = (LinearLayout) findViewById(R.id.eraser_layout);

        mHoverView.switchMode(mHoverView.ERASE_MODE);
        eraserLayout.setVisibility(View.VISIBLE);
        eraserSubButton.setSelected(true);

        back = findViewById(R.id.back);
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private Bitmap getBitmap(String path) {
        Uri uri = getImageUri(path);
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 1024;
            in = mContentResolver.openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;

            BitmapFactory.decodeStream(in, null, o);
            in.close();

            int scale = 1;
            if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
                scale = (int) Math.pow(2, (int) Math
                        .round(Math.log(IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            in = mContentResolver.openInputStream(uri);
            Bitmap b = BitmapFactory.decodeStream(in, null, o2);
            in.close();

            b = Bitmap.createBitmap(b, 0, 0, o2.outWidth, o2.outHeight, getOrientationMatrix(path), true);

            return b;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private Uri getImageUri(String path) {
        return Uri.fromFile(new File(path));
    }

    private Matrix getOrientationMatrix(String path) {
        Matrix matrix = new Matrix();
        ExifInterface exif;
        try {
            exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                    matrix.setScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;
                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                    matrix.setRotate(180);
                    matrix.postScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_TRANSPOSE:
                    matrix.setRotate(90);
                    matrix.postScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_TRANSVERSE:
                    matrix.setRotate(-90);
                    matrix.postScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.setRotate(-90);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return matrix;
    }


    public void resetSubEraserButtonState() {
        eraserSubButton.setSelected(false);
        unEraserSubButton.setSelected(false);
    }

    public void resetBrushButtonState() {
        brushSize1Button.setSelected(false);
        brushSize2Button.setSelected(false);
        brushSize3Button.setSelected(false);
        brushSize4Button.setSelected(false);
    }

    public void updateUndoButton() {
        if(mHoverView.checkUndoEnable()) {
            undoButton.setEnabled(true);
            undoButton.setAlpha(1.0f);
        }
        else {
            undoButton.setEnabled(false);
            undoButton.setAlpha(0.3f);
        }
    }

    public void updateRedoButton() {
        if(mHoverView.checkRedoEnable()) {
            redoButton.setEnabled(true);
            redoButton.setAlpha(1.0f);
        }
        else {
            redoButton.setEnabled(false);
            redoButton.setAlpha(0.3f);
        }
    }

    @Override
    public void onClick(View v) {
        updateUndoButton();
        updateRedoButton();

        switch (v.getId()) {

            case R.id.erase_sub_button:
                mHoverView.switchMode(HoverView.ERASE_MODE);
                resetSubEraserButtonState();
                eraserSubButton.setSelected(true);
                break;
            case R.id.unerase_sub_button:
                mHoverView.switchMode(HoverView.UNERASE_MODE);
                resetSubEraserButtonState();
                unEraserSubButton.setSelected(true);
                break;

            case R.id.brush_size_x_button:
                resetBrushButtonState();
                mHoverView.setEraseOffset(10);
                brushSizexButton.setSelected(true);
                break;

            case R.id.brush_size_0_button:
                resetBrushButtonState();
                mHoverView.setEraseOffset(20);
                brushSize0Button.setSelected(true);
                break;

            case R.id.brush_size_1_button:
                resetBrushButtonState();
                mHoverView.setEraseOffset(40);
                brushSize1Button.setSelected(true);
                break;

            case R.id.brush_size_2_button:
                resetBrushButtonState();
                mHoverView.setEraseOffset(60);
                brushSize2Button.setSelected(true);
                break;

            case R.id.brush_size_3_button:
                resetBrushButtonState();
                mHoverView.setEraseOffset(80);
                brushSize3Button.setSelected(true);
                break;

            case R.id.brush_size_4_button:
                resetBrushButtonState();
                mHoverView.setEraseOffset(100);
                brushSize4Button.setSelected(true);
                break;

            case R.id.nextButton:
//                Intent intent = new Intent(getApplicationContext(), PositionActivity.class);
//                intent.putExtra("imagePath", mHoverView.save());
//                startActivity(intent);

                //Bitmap bitmap = mHoverView.saveDrawnBitmap();

                Intent intentx = new Intent();
                intentx.putExtra("path", ""+mHoverView.save());
                setResult(Activity.RESULT_OK,intentx);
                finish();

                break;

            case R.id.undoButton:
                mHoverView.undo();
                if(mHoverView.checkUndoEnable()) {
                    undoButton.setEnabled(true);
                    undoButton.setAlpha(1.0f);
                }
                else {
                    undoButton.setEnabled(false);
                    undoButton.setAlpha(0.3f);
                }
                updateRedoButton();
                break;
            case R.id.redoButton:
                mHoverView.redo();
                updateUndoButton();
                updateRedoButton();
                break;
        }

    }

}
